package rockPaperScissors;

import java.util.*;

public class RockPaperScissors {

    public static void main(String[] args) {
        /*
         * The code here does two things:
         * It first creates a new RockPaperScissors -object with the
         * code `new RockPaperScissors()`. Then it calls the `run()`
         * method on the newly created object.
         */
        new RockPaperScissors().run();
    }




    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    public static String playerMove() {
        Scanner sc = new Scanner(System.in);
        String playerChoice = sc.nextLine();
        playerChoice = playerChoice.toLowerCase();
        if(playerChoice.equals("rock") || playerChoice.equals("paper") || playerChoice.equals("scissors")) {
            return playerChoice;
        }
        else {
            System.out.println("Sorry, that's not a valid move, please try again.");
            return "Invalid input";
        }

    }

    public String computerMove(){
        Random random = new Random();
        int index = new Random().nextInt(rpsChoices.size());
        String computerChoice = rpsChoices.get(index);
        return computerChoice;
    }

    public void run() {
        // TODO: Implement Rock Paper Scissors

       while(true){
           System.out.println("Let's play round " + roundCounter);
           System.out.println("Your choice (Rock/Paper/Scissors)?\n");
           String playerMove = playerMove();
           String computerMove = computerMove();

           if(playerMove.equals(computerMove)){
               System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". It's a tie!\n");
               System.out.println("Score: human " + humanScore + ", computer " + computerScore + "\n");
           }
           if(playerMove.equals("rock") && computerMove.equals("paper")){
               System.out.println("Human chose rock, computer chose paper. Computer wins!\n");
               computerScore++;
               System.out.println("Score: human " + humanScore + ", computer " + computerScore + "\n");
           }
           else if(playerMove.equals("rock") && computerMove.equals("scissors")) {
               System.out.println("Human chose rock, computer chose scissors. Human wins!\n");
               humanScore++;
               System.out.println("Score: human " + humanScore + ", computer " + computerScore + "\n");
           }
           else if(playerMove.equals("paper") && computerMove.equals("rock")){
               System.out.println("Human chose paper, computer chose rock. Human wins!\n");
               humanScore++;
               System.out.println("Score: human " + humanScore + ", computer " + computerScore + "\n");
           }
           else if(playerMove.equals("paper") && computerMove.equals("scissors")) {
               System.out.println("Human chose paper, computer chose scissors. Computer wins!\n");
               computerScore++;
               System.out.println("Score: human " + humanScore + ", computer " + computerScore + "\n");
           }
           else if(playerMove.equals("scissors") && computerMove.equals("rock")){
               System.out.println("Human chose scissors, computer chose rock. Computer wins!\n");
               computerScore++;
               System.out.println("Score: human " + humanScore + ", computer " + computerScore + "\n");
           }
           else if(playerMove.equals("scissors") && computerMove.equals("paper")) {
               System.out.println("Human chose scissors, computer chose paper. Human wins!\n");
               humanScore++;
               System.out.println("Score: human " + humanScore + ", computer " + computerScore + "\n");
           }
           roundCounter++;
           System.out.println("Do you wish to continue playing? (y/n)?\n");
           String continueAnswer = sc.nextLine();
           if(continueAnswer.equals("n")){
               System.out.println("Bye bye! :)");
               break;
           }
       }

    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
